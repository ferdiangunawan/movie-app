import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/user.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

@TestOn('vm')
void main() {
  group('Sqflite unit test', () {
// Init ffi loader if needed.
    sqfliteFfiInit();
    test('test basic functional of sqflite insert, get with user ', () async {
      var user =
          User(email: 'gugun@gmail.com', password: '123123', userName: 'gugun');
      var factory = databaseFactoryFfi;
      var db = await factory.openDatabase(inMemoryDatabasePath,
          options: OpenDatabaseOptions(
              version: 1,
              onCreate: (db, version) async {
                await db.execute(
                    'CREATE TABLE User (id INTEGER PRIMARY KEY, email TEXT, username TEXT, password TEXT)');
              }));
      // Insert some data
      await db.insert('User', user.toJson());

      // Check content
      expect(await db.query('User'), [
        {
          'id': 1,
          'email': 'gugun@gmail.com',
          'username': 'gugun',
          'password': '123123'
        }
      ]);

      await db.close();
    });
  });
}
