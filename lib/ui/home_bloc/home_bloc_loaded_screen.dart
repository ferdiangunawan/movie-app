import 'package:flutter/material.dart';

import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/detail_movie_page/detail_movie_page.dart';
import 'package:majootestcase/utils/custom_navigator.dart';

import 'home_movie_widget.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data?.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return MovieItemWidget(
          data: data![index],
          onTap: () {
            CustomNavigator.push(
                context,
                DetailMoviePage(
                  data: data![index],
                  heroTag: '$index',
                ));
          },
          heroTags: '$index',
        );
      },
    );
  }
}
