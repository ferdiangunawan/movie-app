import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../common/widget/custom_appbar.dart';
import '../../common/widget/header_search.dart';
import '../../services/local/user_local_services.dart';
import 'home_bloc_loaded_screen.dart';
import '../../common/widget/loading.dart';
import '../../common/widget/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  HomeBlocScreen({Key? key}) : super(key: key);

  final searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var homeBloc = context.read<HomeBlocCubit>();
    return Scaffold(
      appBar: CustomAppbar.appBar(
        context: context,
        title: 'Movie App',
        actions: [
          IconButton(
              onPressed: () {
                UserServices.logout(context);
              },
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          homeBloc.fetchingData(query: searchController.text);
        },
        child: SingleChildScrollView(
          child: Column(
            children: [
              Header(
                onFieldSubmitted: (String value) {
                  homeBloc.fetchingData(query: value);
                },
                onTap: () {
                  homeBloc.fetchingData(query: searchController.text);
                },
                searchController: searchController,
              ),
              BlocBuilder<HomeBlocCubit, HomeBlocState>(
                  builder: (context, state) {
                if (state is HomeBlocLoadedState) {
                  return HomeBlocLoadedScreen(data: state.data);
                } else if (state is HomeBlocInitialState) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      LoadingIndicator(),
                    ],
                  );
                } else if (state is HomeBlocErrorState) {
                  return ErrorScreen(
                    message: state.error.toString(),
                    retry: () {
                      homeBloc.fetchingData(query: searchController.text);
                    },
                  );
                } else if (state is HomeBlocInternetBadState) {
                  return ErrorScreen(
                    message: state.error,
                    isBadConnection: true,
                    retry: () {
                      homeBloc.fetchingData(query: searchController.text);
                    },
                  );
                }

                return Center(
                    child:
                        Text(kDebugMode ? "state not implemented $state" : ""));
              }),
            ],
          ),
        ),
      ),
    );
  }
}
