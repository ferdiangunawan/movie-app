import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_image.dart';

import '../../common/theme/theme.dart';
import '../../models/movie_response.dart';

class MovieItemWidget extends StatelessWidget {
  final Data data;
  final String heroTags;
  final Function() onTap;
  const MovieItemWidget({
    Key? key,
    required this.data,
    required this.heroTags,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(18.0))),
        child: Column(
          children: [
            Hero(
                tag: heroTags,
                child: CustomImage(
                    imageUrl: data.i?.imageUrl ?? '',
                    width: 200,
                    height: 300,
                    radius: 18)),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Center(
                      child: Text(data.title ?? '',
                          style:
                              CustomFont.blackFontStyle.copyWith(fontSize: 15),
                          textAlign: TextAlign.center))),
            ),
            Divider()
          ],
        ),
      ),
    );
  }
}
