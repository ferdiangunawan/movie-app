import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/login_bloc/login_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/custom_snackbar.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({
    Key? key,
  }) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  final _userNameController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool isLoading = false;
  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Selamat Datang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: colorBlue,
                ),
              ),
              Text(
                'Silahkan daftar terlebih dahulu',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                text: 'Register',
                isLoading: isLoading,
                onPressed: handleRegister,
                height: 100,
              ),
              SizedBox(
                height: 10,
              ),
              _login()
            ],
          ),
        ),
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          context.read<LoginBlocCubit>().loginRoute();
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister() async {
    final form = formKey.currentState;
    final String? _email = _emailController.textController.text;
    final String? _password = _passwordController.textController.text;
    final String? _userName = _userNameController.textController.text;

    if (_email == '' || _password == '' || _userName == '') {
      CustomSnackbar.show(context, 'Form tidak boleh kosong');
    } else {
      var registerBloc = BlocProvider.of<LoginBlocCubit>(context);

      if (form!.validate() &&
          _email != null &&
          _password != null &&
          _userName != null) {
        form.save();

        var user =
            User(userName: _userName, email: _email, password: _password);
        var message = await registerBloc.registerUser(user);
        CustomSnackbar.show(context, message);
      } else {
        CustomSnackbar.show(context, 'Mohon masukan form dengan valid!');
      }
    }
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            textInputAction: TextInputAction.done,
            context: context,
            controller: _userNameController,
            isEmail: true,
            hint: 'Username',
            label: 'User Name',
          ),
          CustomTextFormField(
            textInputAction: TextInputAction.done,
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (String? val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              return pattern.hasMatch(val!) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            textInputAction: TextInputAction.done,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _userNameController.dispose();

    super.dispose();
  }
}
