import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_image.dart';

import 'package:majootestcase/common/theme/theme.dart';

import '../../common/widget/custom_appbar.dart';
import '../../models/movie_response.dart';

class DetailMoviePage extends StatelessWidget {
  final Data data;
  final String heroTag;
  const DetailMoviePage({
    Key? key,
    required this.data,
    required this.heroTag,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppbar.appBar(context: context, title: 'Detail Movie'),
        body: ListView(
          children: [
            Column(
              children: [
                Hero(
                  tag: heroTag,
                  child: Container(
                    color: Colors.black,
                    height: 250,
                    width: double.infinity,
                    child: Image.network(
                      data.i?.imageUrl ?? '',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    data.title ?? '',
                    style: CustomFont.blackFontStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  data.year.toString(),
                  style: CustomFont.blackFontStyle.copyWith(fontSize: 14),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: data.series?.length ?? 0,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: EdgeInsets.only(top: index == 0 ? 0 : 15),
                        child: ListTile(
                          leading: CustomImage(
                            imageUrl: data.series![index].i?.imageUrl ?? '',
                            width: 70,
                            height: 60,
                            radius: 10,
                          ),
                          // ClipRRect(
                          //   borderRadius: BorderRadius.circular(10),
                          //   child: Image.network(
                          //     data.series![index].i?.imageUrl ?? '',
                          //     height: 60,
                          //     width: 70,
                          //     fit: BoxFit.cover,
                          //   ),
                          // ),
                          title: Text(
                            data.series![index].titleSeries ?? '',
                            style: CustomFont.blackFontStyle
                                .copyWith(fontSize: 17),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
