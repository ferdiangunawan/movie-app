import 'package:flutter/material.dart';

class CustomFont {
  static TextStyle blackFontStyle = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static TextStyle whiteFontStyle =
      TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white
          // color: colorBlue,
          );
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
