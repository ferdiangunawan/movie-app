import 'package:flutter/material.dart';
import 'package:majootestcase/common/theme/theme.dart';

class CustomAppbar {
  static AppBar appBar(
      {required BuildContext context,
      required String title,
      Function()? onTap,
      List<Widget>? actions}) {
    return AppBar(
      title: Text(
        title,
        style: CustomFont.blackFontStyle.copyWith(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      // leading: IconButton(
      //   icon: Icon(Icons.arrow_back_ios),
      //   onPressed: () => onTap != null ? onTap() : Navigator.of(context).pop(),
      // ),
      actions: actions ?? [],
      centerTitle: true,
      titleSpacing: 0,
      backgroundColor: Colors.black,
    );
  }
}
