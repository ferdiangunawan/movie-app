import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final bool isBadConnection;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.isBadConnection = false,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 20),
            height: 120,
            width: 120,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(isBadConnection
                        ? 'assets/bad_connection.png'
                        : 'assets/404.png'),
                    fit: BoxFit.contain)),
          ),
          Text(
            message,
            style: TextStyle(fontSize: 12, color: textColor ?? Colors.black),
          ),
          retry != null
              ? Column(
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    retryButton ??
                        IconButton(
                          onPressed: () {
                            retry!();
                          },
                          icon: Icon(
                            Icons.refresh_sharp,
                            color: Colors.black,
                          ),
                        ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text('Retry')
                  ],
                )
              : SizedBox()
        ],
      ),
    );
  }
}
