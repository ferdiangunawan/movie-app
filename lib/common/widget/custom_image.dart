import 'package:flutter/material.dart';

class CustomImage extends StatelessWidget {
  final String imageUrl;
  final double width;
  final double height;
  final BoxFit? boxFit;
  final double radius;

  const CustomImage({
    Key? key,
    required this.imageUrl,
    required this.width,
    required this.height,
    this.boxFit,
    required this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            color: Colors.grey.shade200,
            image: DecorationImage(
                image: NetworkImage(imageUrl), fit: boxFit ?? BoxFit.cover)));
  }
}
