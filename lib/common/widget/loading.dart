import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final double height;
  final Color? color;

  const LoadingIndicator(
      {Key? key, this.height = 10, this.color = Colors.black})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 5,
        color: color,
      ),
    );
  }
}
