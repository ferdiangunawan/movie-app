import 'package:flutter/material.dart';
import 'package:majootestcase/common/theme/theme.dart';

class Header extends StatelessWidget {
  final TextEditingController searchController;
  final Function() onTap;
  final Function(String value) onFieldSubmitted;
  const Header({
    Key? key,
    required this.searchController,
    required this.onTap,
    required this.onFieldSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5,
                            spreadRadius: 1,
                            offset: Offset(3, 1),
                          )
                        ],
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white),
                    width: 250,
                    child: TextFormField(
                        onFieldSubmitted: onFieldSubmitted,
                        cursorColor: Colors.black,
                        controller: searchController,
                        decoration: InputDecoration(
                            isDense: true,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: Colors.transparent),
                                borderRadius: BorderRadius.circular(8)),
                            hintStyle: CustomFont.blackFontStyle
                                .copyWith(color: Colors.grey),
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            hintText: 'Search here :)')),
                  ),
                  const SizedBox(width: 15),
                  InkWell(
                      onTap: onTap,
                      child: Container(
                          width: 45,
                          height: 45,
                          decoration: BoxDecoration(
                              boxShadow: const [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 5,
                                  spreadRadius: 1,
                                  offset: Offset(3, 1),
                                )
                              ],
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white),
                          child: const Icon(Icons.search,
                              size: 30, color: Colors.grey)))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
