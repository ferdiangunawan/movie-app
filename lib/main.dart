import 'package:majootestcase/bloc/login_bloc/login_bloc_cubit.dart';
import 'package:majootestcase/common/widget/loading.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/ui/login/register_page.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
        ),
        BlocProvider(
          create: (context) => LoginBlocCubit(),
        ),
        BlocProvider(
          create: (context) => HomeBlocCubit(),
        )
      ],
      child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: MyHomePageScreen()),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(builder: (context, state) {
      if (state is AuthBlocLoginState) {
        context.read<LoginBlocCubit>().loginRoute();
        return BlocBuilder<LoginBlocCubit, LoginBlocState>(
          builder: (context, state) {
            if (state is LoginBlocSuccesState) {
              context.read<HomeBlocCubit>().fetchingData();
              return HomeBlocScreen();
            } else if (state is LoginRouteState) {
              return LoginPage();
            } else if (state is RegisterRouteState) {
              return RegisterPage();
            } else {
              return Center(
                child: Text(kDebugMode ? "state not implemented $state" : ""),
              );
            }
          },
        );
      } else if (state is AuthBlocLoggedInState) {
        return BlocProvider(
          create: (context) => HomeBlocCubit()..fetchingData(),
          child: HomeBlocScreen(),
        );
      } else if (state is AuthBlocInitialState) {
        return LoadingIndicator();
      }

      return Center(
        child: Text(kDebugMode ? "state not implemented $state" : ""),
      );
    });
  }
}
