import 'dart:async';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio dioInstance = Dio();
createInstance() async {
  var options = BaseOptions(
      baseUrl: Api.baseUrl,
      connectTimeout: Api.timout,
      receiveTimeout: Api.timout,
      headers: {
        "x-rapidapi-key": Api.apiKey,
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = Dio(options);
  dioInstance.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio> dio() async {
  await createInstance();
  dioInstance.options.baseUrl = Api.baseUrl;
  return dioInstance;
}
