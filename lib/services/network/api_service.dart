import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/network/dio_config_service.dart'
    as dioConfig;

class ApiServices {
  Future<MovieResponse?> getMovieList({String? query}) async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get("/auto-complete",
          queryParameters: {'q': query ?? 'game of thr'});
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } catch (e) {
      print(e.toString());
      throw Exception('Unknown Error');
    }
  }
}
