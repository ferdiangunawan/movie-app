import 'package:flutter/material.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/custom_navigator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'local_service_helper.dart';

class UserServices {
  static Future<String> login(String email, String password) async {
    String status = "logged";
    var user = User(
      email: email,
      password: password,
    );

    var db = DatabaseHelper();

    var isRegistered = await db.selectUser(user);
    if (isRegistered) {
      status = 'logged';
      return status;
    } else {
      status = 'not';
      return status;
    }
  }

  static void logout(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setBool("is_logged_in", false);
    CustomNavigator.pushAndRemoveUntill(context, MyApp());
  }
}
