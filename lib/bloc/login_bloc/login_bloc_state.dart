part of 'login_bloc_cubit.dart';

abstract class LoginBlocState extends Equatable {
  const LoginBlocState();

  @override
  List<Object> get props => [];
}

class LoginBlocSuccesState extends LoginBlocState {}

class RegisterRouteState extends LoginBlocState {}

class LoginRouteState extends LoginBlocState {}

class LoginBlocLoadedState extends LoginBlocState {
  final data;

  LoginBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}
