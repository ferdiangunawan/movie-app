import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/local/local_service_helper.dart';
import 'package:majootestcase/services/local/user_local_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_bloc_state.dart';

class LoginBlocCubit extends Cubit<LoginBlocState> {
  LoginBlocCubit() : super(LoginRouteState());

  Future<bool> loginUser(User user) async {
    try {
      bool? isSuccess;
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      var isUserRegistered =
          await UserServices.login(user.email!, user.password!);
      if (isUserRegistered == 'logged') {
        await sharedPreferences.setBool("is_logged_in", true);
        isSuccess = true;
        emit(LoginBlocSuccesState());
      } else {
        await sharedPreferences.setBool("is_logged_in", false);
        isSuccess = false;
      }
      return isSuccess;
    } catch (exception) {
      return false;
    }
  }

  Future<String> registerUser(User user) async {
    try {
      String message = 'Berhasil Mendaftar';
      var db = DatabaseHelper();

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      //check dulu apakah sudah pernah mendaftar
      var isUserRegistered = await db.checkRegister(user.email!);
      if (isUserRegistered) {
        message = 'Akun Sudah Terdaftar, Silahkan Login';
      } else {
        await db.saveUser(user);
        //crosscheck apakah berhasil mendaftar
        var isUserRegistered = await db.checkRegister(user.email!);

        if (isUserRegistered) {
          await sharedPreferences.setBool("is_logged_in", true);
          message = 'Berhasil Mendaftar';

          emit(LoginBlocSuccesState());
        } else {
          message = 'Gagal Mendaftar';
        }
      }
      return message;
    } catch (exception) {
      return 'Gagal mendaftar';
    }
  }

  void registerRoute() {
    emit(RegisterRouteState());
  }

  void loginRoute() {
    emit(LoginRouteState());
  }
}
