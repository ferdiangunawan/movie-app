import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/network/api_service.dart';
import 'package:majootestcase/utils/check_connectivity.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> with CheckConnectivity {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData({String? query}) async {
    if (query != '')
      try {
        emit(HomeBlocInitialState());
        var isInternetGood = await tryIsConnectionGood();
        if (isInternetGood) {
          ApiServices apiServices = ApiServices();
          MovieResponse? movieResponse =
              await apiServices.getMovieList(query: query);
          if (movieResponse == null) {
            emit(HomeBlocErrorState("Unknown Error"));
          } else {
            emit(HomeBlocLoadedState(movieResponse.data!));
          }
        } else {
          emit(HomeBlocInternetBadState(
              'Silahkan periksa koneksi Internet anda'));
        }
      } on Exception catch (exception) {
        emit(HomeBlocErrorState(exception));
      }
  }
}
