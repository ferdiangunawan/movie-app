import 'dart:io';

mixin CheckConnectivity {
  Future<bool> tryIsConnectionGood() async {
    try {
      final response = await InternetAddress.lookup('www.google.com');
      if (response.isEmpty) return false;
      return true;
    } on SocketException catch (e) {
      print(e);
      return false;
    }
  }
}
