import 'package:flutter/material.dart';

abstract class CustomNavigator {
  static push(BuildContext context, Widget page, {bool rootNavigator = false}) {
    Navigator.of(context, rootNavigator: rootNavigator)
        .push(MaterialPageRoute(builder: (_) => page));
  }

  static pushReplacement(BuildContext context, Widget page,
      {bool rootNavigator = false}) {
    Navigator.of(context, rootNavigator: rootNavigator)
        .pushReplacement(MaterialPageRoute(builder: (_) => page));
  }

  static pushAndRemoveUntill(BuildContext context, Widget page) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => page),
        (Route<dynamic> route) => false);
  }
}
